//
//  analogViewController.h
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"
#import "dataCell.h"

@interface analogViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIStepper *stepper;

@property (weak, nonatomic) IBOutlet UILabel *labelPin;
@property (weak, nonatomic) IBOutlet UITextField *fieldInterval;

@property (strong, nonatomic) NSMutableArray *keys;
@property (strong, nonatomic) NSMutableArray *data;
@property (strong, nonatomic) NSMutableArray *times;

@property (weak, nonatomic) IBOutlet UIButton *button;

- (IBAction)buttonStartStop:(id)sender;
- (IBAction)stepperChanged:(id)sender;

@property (strong, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (readwrite) bool isRunning;

@property (readwrite) double currentInterval;
@property (readwrite) int runCount;
- (IBAction)viewTouch:(id)sender;

@end
