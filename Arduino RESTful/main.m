//
//  main.m
//  Arduino RESTful
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
