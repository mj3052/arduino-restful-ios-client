//
//  analogViewController.m
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import "analogViewController.h"

@interface analogViewController ()

@end

@implementation analogViewController

@synthesize timer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self resetAllValues];
    
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 135)];
	footer.backgroundColor = [UIColor clearColor];
    
	_tableView.tableFooterView = footer;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getSingleRow
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ip = [defaults objectForKey:@"ip"];

    _runCount = _runCount + 1;
    
    [manager GET:[NSString stringWithFormat:@"http://%@/%@", ip, [self getPinForIndex:[_stepper value]]] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Sent to IP: %@", ip);
        NSLog(@"%@", operation.responseString);
        
        NSDictionary *callback = [NSJSONSerialization JSONObjectWithData:[operation.responseString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:nil];
        
        if([NSString stringWithFormat:@"%@",[[callback allKeys] objectAtIndex:0]] != nil) {
        [_keys addObject:[NSString stringWithFormat:@"%@",[[callback allKeys] objectAtIndex:0]]];
        }
        
        if([callback valueForKey:[[callback allKeys] objectAtIndex:0]] != nil) {
        [_data addObject:[callback valueForKey:[[callback allKeys] objectAtIndex:0]]];
        }

        
        NSLog(@"runs: %d", _runCount);
        NSLog(@"interval: %f", _currentInterval);
        
        double seconds = _runCount * _currentInterval;
        
        [_times addObject:[NSString stringWithFormat:@"%.1f", round(seconds)]];
        NSLog(@"%f", seconds);
        
        [_tableView reloadData];
        
        [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_data count]-1 inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)startTimerWithInterval:(float)interval
{
    double setting;
    
    if(interval == 0) {
        setting = 5.0;
    }
    else {
        setting = interval;
    }
    _currentInterval = setting;
    timer = [NSTimer scheduledTimerWithTimeInterval: setting
                                                  target: self
                                                selector:@selector(getSingleRow)
                                                userInfo: nil repeats:YES];
}

- (void)stopTimer
{
    [timer invalidate];
}

- (void)resetAllValues
{
    _runCount = 0;
    _data = [NSMutableArray array];
    _keys = [NSMutableArray array];
    _times = [NSMutableArray array];
    _currentInterval = 5;
    [_tableView reloadData];
}

- (NSString *)getPinForIndex:(int)index
{
    return [NSString stringWithFormat:@"A%d", index];
}


- (IBAction)buttonStartStop:(id)sender {
    [self.view endEditing:YES];
    
    if(_isRunning) {
        [self stopTimer];
        [_button setTitle:@"Start loading" forState:UIControlStateNormal];
        _isRunning = NO;
    }
    else {
        [self stopTimer];
        [self resetAllValues];
        [_tableView reloadData];
        float interval = [_fieldInterval.text intValue];
       [self startTimerWithInterval:interval];
        [_button setTitle:@"Stop loading" forState:UIControlStateNormal];
        _isRunning = YES;
    }
}

- (IBAction)stepperChanged:(id)sender {
    [_labelPin setText:[self getPinForIndex:[_stepper value]]];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count = [_data count];
    NSLog(@"Count: %d", count);
    NSLog(@"%@", _data);
    return count;
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(motion == UIEventSubtypeMotionShake)
    {
        NSLog(@"DID SHAKE DEVICE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change IP of device" message:@"Do you want to change the ip of the device?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        
        [alert show];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Alert end");
    NSLog(@"%i", buttonIndex);
    
    if(buttonIndex == 1) {
        [self performSegueWithIdentifier:@"ip" sender:self];
    }
    _isRunning = NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"dataCell";
    
    dataCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    [cell.labelNumber setText:[NSString stringWithFormat:@"%@", [_keys objectAtIndex:indexPath.row]]];
    
    [cell.labelData setText:[NSString stringWithFormat:@"%@", [_data objectAtIndex:indexPath.row]]];
    
    [cell.labelTime setText:[NSString stringWithFormat:@"%@s", [_times objectAtIndex:indexPath.row]]];
    
    
    
    // Configure the cell...
    
    return cell;
}

- (IBAction)viewTouch:(id)sender {
    [self.view endEditing:YES];
    
}
@end
