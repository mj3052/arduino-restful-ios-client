//
//  dataCell.h
//  Arduino RESTful
//
//  Created by Mathias Jensen on 15/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dataCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelNumber;
@property (weak, nonatomic) IBOutlet UILabel *labelData;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

@end
