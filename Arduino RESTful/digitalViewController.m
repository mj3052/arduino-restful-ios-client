//
//  digitalViewController.m
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import "digitalViewController.h"

@interface digitalViewController ()

@end

@implementation digitalViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //Get the delegate
    _delegate = [[UIApplication sharedApplication] delegate];
    _currentPin = [_stepper value];
    [_pinLabel setText:[NSString stringWithFormat:@"%d", _currentPin]];
	NSLog(@"Currently using IP: %@", _delegate.ip);
    
    double delayInSeconds = 2.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if([defaults objectForKey:@"ip"] == nil) {
            [self performSegueWithIdentifier:@"ip" sender:self];
        }
    });


}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stepperChange:(id)sender {
    _currentPin = [_stepper value];
    [_pinLabel setText:[NSString stringWithFormat:@"%d", _currentPin]];
    [_segment setSelectedSegmentIndex:-1];
}

- (IBAction)stateChanged:(id)sender {
    [self sendPinState:[_segment selectedSegmentIndex] toPin:_currentPin];
}


- (void)sendPinState:(int)state toPin:(int)pin
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ip = [defaults objectForKey:@"ip"];
    
    NSString *stateString;
    if(state == 1) {
        stateString = @"HIGH";
    } else {
        stateString = @"LOW";
    }
    
    [manager GET:[NSString stringWithFormat:@"http://%@/%d/%@", ip, _currentPin, stateString] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Sent to IP: %@", ip);
        [_segment setSelectedSegmentIndex:-1];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(motion == UIEventSubtypeMotionShake)
    {
        NSLog(@"DID SHAKE DEVICE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change IP of device" message:@"Do you want to change the ip of the device?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        
        [alert show];
        
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        [self performSegueWithIdentifier:@"ip" sender:self];
    }
    
}

@end
