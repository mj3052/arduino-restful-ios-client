//
//  dataCell.m
//  Arduino RESTful
//
//  Created by Mathias Jensen on 15/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import "dataCell.h"

@implementation dataCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
