//
//  PWMViewController.m
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import "PWMViewController.h"

@interface PWMViewController ()

@end

@implementation PWMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _delegate = [[UIApplication sharedApplication] delegate];
    
    _currentPin = [_stepper value];
    [_pinLabel setText:[NSString stringWithFormat:@"%d", _currentPin]];
    
    int round = lroundf([_slider value]);
    [_PWMLabel setText:[NSString stringWithFormat:@"%d", round]];
    
    [self stopSpinner];
    
	NSLog(@"%@", _delegate.ip);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dragInside:(id)sender {
    long round = lroundf([_slider value]);
    [_PWMLabel setText:[NSString stringWithFormat:@"%ld", round]];
}

- (IBAction)stepperChange:(id)sender {
    _currentPin = [_stepper value];
    [_pinLabel setText:[NSString stringWithFormat:@"%d", _currentPin]];
}

- (IBAction)slideChange:(id)sender {
    long round = lroundf([_slider value]);
    [_PWMLabel setText:[NSString stringWithFormat:@"%ld", round]];
}

- (IBAction)sendDown:(id)sender {
    long round = lroundf([_slider value]);
    
    [self sendPWM:round toPin:_currentPin];
    
}

- (void)sendPWM:(long)PWM toPin:(int)pin
{
    [self startSpinner];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *ip = [defaults objectForKey:@"ip"];
    
    [manager GET:[NSString stringWithFormat:@"http://%@/%d/%ld", ip, _currentPin, PWM] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [_activity setHidden:YES];
        [_activity stopAnimating];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self stopSpinner];
    }];
}

-(void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if(motion == UIEventSubtypeMotionShake)
    {
        NSLog(@"DID SHAKE DEVICE");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Change IP of device" message:@"Do you want to change the ip of the device?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        
        [alert show];
        
    }
}

- (void)startSpinner {
    [_activity setHidden:NO];
    [_activity startAnimating];
}

- (void)stopSpinner {
    [_activity setHidden:YES];
    [_activity stopAnimating];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Alert end");
    NSLog(@"%i", buttonIndex);
    
    if(buttonIndex == 1) {
        [self performSegueWithIdentifier:@"ip" sender:self];
    }

}

@end
