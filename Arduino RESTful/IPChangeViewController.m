//
//  IPChangeViewController.m
//  Arduino RESTful
//
//  Created by Mathias Jensen on 15/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import "IPChangeViewController.h"

@interface IPChangeViewController ()

@end

@implementation IPChangeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults valueForKey:@"ip"] != nil) {
        [_FieldIP setText:[defaults valueForKey:@"ip"]];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonSave:(id)sender {
    if([_FieldIP.text length] > 7) {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_FieldIP.text forKey:@"ip"];
    [defaults synchronize];
    [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No known IP" message:@"You need to enter the IP of your Arduino" delegate:self cancelButtonTitle:@"Okay :(" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)buttonCancel:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults valueForKey:@"ip"] != nil) {
    [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"No known IP" message:@"You need to enter the IP of your Arduino" delegate:self cancelButtonTitle:@"Okay :(" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

- (IBAction)buttonResign:(id)sender {
    [self.view endEditing:YES];
}

@end
