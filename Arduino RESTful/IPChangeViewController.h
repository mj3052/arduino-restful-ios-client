//
//  IPChangeViewController.h
//  Arduino RESTful
//
//  Created by Mathias Jensen on 15/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPChangeViewController : UIViewController

// Outlets
@property (weak, nonatomic) IBOutlet UITextField *FieldIP;

// Actions
- (IBAction)buttonSave:(id)sender;
- (IBAction)buttonCancel:(id)sender;
- (IBAction)buttonResign:(id)sender;

@end
