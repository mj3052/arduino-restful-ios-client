//
//  digitalViewController.h
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"

@interface digitalViewController : UIViewController

@property (strong, nonatomic) AppDelegate *delegate;
@property (readwrite) int currentPin;

// Outlets
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;

// Actions
- (IBAction)stepperChange:(id)sender;

- (IBAction)stateChanged:(id)sender;

@end
