//
//  PWMViewController.h
//  RESTful Arduino
//
//  Created by Mathias Jensen on 13/11/13.
//  Copyright (c) 2013 Pixolink. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "AFHTTPRequestOperationManager.h"

@interface PWMViewController : UIViewController
@property (strong, nonatomic) AppDelegate *delegate;
@property (readwrite) int currentPin;

@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UILabel *pinLabel;

- (IBAction)dragInside:(id)sender;
- (IBAction)stepperChange:(id)sender;
- (IBAction)slideChange:(id)sender;
- (IBAction)sendDown:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *PWMLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@end
